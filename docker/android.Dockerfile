# https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Android.gitlab-ci.yml


FROM openjdk:8-jdk

ENV ANDROID_COMPILE_SDK=26 \
    ANDROID_BUILD_TOOLS=24.0.0 \
    ANDROID_SDK_TOOLS=24.4.1 \
    ANDROID_HOME=$PWD/android-sdk-linux \
    PATH=$PATH:$PWD/android-sdk-linux/platform-tools/:$PWD/android-sdk-linux/tools/:$PWD/android-sdk-linux/tools/bin/

RUN apt-get --quiet update --yes \
    && apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 \
    && apt-get upgrade -y \
    && apt-get autoremove -y

RUN wget --quiet --output-document=android-sdk.tgz https://dl.google.com/android/android-sdk_r${ANDROID_SDK_TOOLS}-linux.tgz \
    && tar --extract --gzip --file=android-sdk.tgz \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter android-${ANDROID_COMPILE_SDK} \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter platform-tools \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter build-tools-${ANDROID_BUILD_TOOLS} \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-android-m2repository \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-google_play_services \
    && echo y | android-sdk-linux/tools/android --silent update sdk --no-ui --all --filter extra-google-m2repository \
    && mkdir android-sdk-linux/licenses \
    && printf "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e" > android-sdk-linux/licenses/android-sdk-license \
    && printf "84831b9409646a918e30573bab4c9c91346d8abd" > android-sdk-linux/licenses/android-sdk-preview-license
