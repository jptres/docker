FROM bitnami/postgresql:10.7.0

USER root

#RUN apt update && apt install -y postgis
ENV POSTGIS_VERSION 2.5.1

## Install Postgis
RUN install_packages wget gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev \
    && cd /tmp \
    && wget "http://download.osgeo.org/postgis/source/postgis-2.4.0.tar.gz" \
    && export C_INCLUDE_PATH=/opt/bitnami/postgresql/include/:/opt/bitnami/common/include/ \
    && export LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
    && export LD_LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ \
    && tar zxf postgis-2.4.0.tar.gz && cd postgis-2.4.0 \
    && ./configure --with-pgconfig=/opt/bitnami/postgresql/bin/pg_config \
    && make \
    && make install

USER 1001


#RUN mkdir -p /var/lib/apt/lists/partial && \
#      apt-get update && \
#      apt-get install wget gcc make build-essential libxml2-dev libgeos-dev libproj-dev libgdal-dev -y && \
#      su - postgres && \
#      cd /tmp && \
#      wget "http://download.osgeo.org/postgis/source/postgis-2.4.6.tar.gz" && \
#      export C_INCLUDE_PATH=/opt/bitnami/postgresql/include/:/opt/bitnami/common/include/ && \
#      export LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ && \
#      export LD_LIBRARY_PATH=/opt/bitnami/postgresql/lib/:/opt/bitnami/common/lib/ && \
#      tar zxf postgis-2.4.6.tar.gz && cd postgis-2.4.6 && \
#      ./configure --with-pgconfig=/opt/bitnami/postgresql/bin/pg_config && \
#      make && \
#      make install
