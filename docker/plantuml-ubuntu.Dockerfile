FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -qy openssh-client wget git openjdk-8-jdk graphviz
