FROM registry.gitlab.com/jptres/docker/android

ENV NODEJS_VERSION=8.11.3 \
    CORDOVA_VERSION=8.0.0 \
    PATH=$PATH:/opt/node/bin

WORKDIR "/opt/node"

RUN apt-get update && apt-get install -y curl git ca-certificates gradle --no-install-recommends && \
    curl -sL https://nodejs.org/dist/v${NODEJS_VERSION}/node-v${NODEJS_VERSION}-linux-x64.tar.gz | tar xz --strip-components=1 && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

RUN npm i -g --unsafe-perm cordova@${CORDOVA_VERSION}