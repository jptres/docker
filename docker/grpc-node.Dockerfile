#
# grpc/node image with grpc gateway install
#
# 1. Install go
# 2. Download grpc gateway
# 3. Install typescript plugin
# 4. Export env vars for later uses
#

FROM grpc/node:1.0

ENV GRPC_VERSION=v1.19.0
ENV GO_VERSION=1.11
ENV GOPATH=/root/go
ENV NPM_GOOGLE_PROTOBUF_VERSION=3.8.0
ENV PATH="$PATH:/usr/local/go/bin:$GOPATH/bin:/usr/local/lib/node_modules/grpc-tools/bin:/root/grpc/src/proto"

WORKDIR /root

# Clone gRPC
#RUN git clone https://github.com/grpc/grpc.git && \
#    cd grpc && git checkout tags/${GRPC_VERSION} && cd - && \
#    ln -s /root/grpc/src/proto/grpc/ /usr/local/include/

# Install Go
RUN wget https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz 
    
# Download go packages
RUN go get -u google.golang.org/grpc && \
    go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway && \
    go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger && \
    go get -u github.com/golang/protobuf/protoc-gen-go

# Install npm packages
RUN npm install -g ts-protoc-gen google-protobuf@${NPM_GOOGLE_PROTOBUF_VERSION}
